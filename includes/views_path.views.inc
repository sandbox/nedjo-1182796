<?php

/**
 * Implementation of hook_views_data()
 */
function views_path_views_data() {
  $data['term_data']['path'] = array(
    'field' => array(
      'title' => t('Term path'),
      'help' => t('The aliased path to this term.'),
      'handler' => 'views_handler_field_term_path',
    ),
  );
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function views_path_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_path') . '/includes',
    ),
    'handlers' => array(
      'views_handler_field_term_path' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
